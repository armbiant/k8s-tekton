
kubectl apply -f hello-world.yaml
kubectl apply -f hello-world-run.yaml
kubectl apply -f goodbye-world.yaml
kubectl apply -f hello-goodbye-pipeline.yaml
kubectl apply -f hello-goodbye-pipeline-run.yaml
kubectl apply -f trigger-template.yaml
kubectl apply -f trigger-binding.yaml
kubectl apply -f rbac.yaml
kubectl apply -f event-listener.yaml

kubectl port-forward service/el-hello-listener 8080

curl -v \
   -H 'content-Type: application/json' \
   -d '{"git-revision": "Tekton"}' \
   http://localhost:8080

kubectl port-forward service/el-dochub-listener 8080

curl -v \
   -H 'content-Type: application/json' \
   -d '{"repo-url": "Tekton"}' \
   http://localhost:8080
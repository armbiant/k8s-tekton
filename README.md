# k8s-tekton

```bash
k config use-context prod

kubectl apply --filename https://storage.googleapis.com/tekton-releases/pipeline/latest/release.yaml
kubectl apply --filename https://storage.googleapis.com/tekton-releases/dashboard/latest/release.yaml
kubectl apply --filename https://storage.googleapis.com/tekton-releases/triggers/latest/release.yaml
kubectl apply --filename https://storage.googleapis.com/tekton-releases/triggers/latest/interceptors.yaml

kubectl config set-context --current --namespace tekton-pipelines

kubectl apply -f extra/cert-tekton.yaml
k get event -w



k create ns tekton-dev

kubectl config set-context --current --namespace tekton-dev
kubectl apply -f extra/cert-tekton-event.yaml
k get event -w

kubectl apply -f extra/traefik-ingress.yaml

kubectl apply -f tekton-triggers-org/hello-world.yaml
kubectl apply -f tekton-triggers-org/hello-world-run.yaml
kubectl apply -f tekton-triggers-org/goodbye-world.yaml
kubectl apply -f tekton-triggers-org/hello-goodbye-pipeline.yaml
kubectl apply -f tekton-triggers-org/hello-goodbye-pipeline-run.yaml

tkn pipelinerun logs hello-goodbye-run -f -n tekton-dev
kubectl apply -f tekton-triggers-org/trigger-template.yaml
kubectl apply -f tekton-triggers-org/trigger-binding.yaml
kubectl apply -f tekton-triggers-org/rbac.yaml
kubectl apply -f tekton-triggers-org/event-listener.yaml

kubectl port-forward service/el-hello-listener 8080

curl -v \
   -H 'content-Type: application/json' \
   -d '{"git-http-url":"url1111"}' \
   https://tekton.expertjob.online/event

curl -v \
    -H 'X-GitLab-Token: 1234567' \
    -H 'X-Gitlab-Event: Push Hook' \
    -H 'Content-Type: application/json' \
    --data-binary "@gitlab-push-event.json" \
    https://tekton.expertjob.online/event


kubectl port-forward -n tekton-pipelines service/tekton-dashboard 9097:9097

tkn hub install task git-clone && tkn hub install task buildah
tkn hub install task git-clone --version 0.9

kubectl apply -f manifests/git-clone.yaml
kubectl delete -f manifests/git-clone.yaml

kubectl apply -f https://api.hub.tekton.dev/v1/resource/tekton/task/buildah/0.5/raw
kubectl delete -f https://api.hub.tekton.dev/v1/resource/tekton/task/buildah/0.5/raw

tkn task list
tkn pipelinerun delete --all

StartUp
kubectl apply -f manifests/secret.yaml
kubectl apply -f manifests/pipeline.yaml
kubectl apply -f manifests/pipelinerun.yaml

Prod for typescriptdemosite
kubectl apply -f tekton-triggers-org/


kubectl apply -f kubernetes-actions/action-arm64.yaml
kubectl delete -f kubernetes-actions/action-arm64.yaml

kubectl apply -f  kubernetes-actions/role.yaml

kubectl create -f kubernetes-actions/tstrun.yaml

kubectl create -f kubernetes-actions/customtask.yaml
k create -f kubernetes-actions/custometaskrun.yaml

tkn taskrun delete build-images && tkn task delete build-images && k create -f kubernetes-actions/customtask.yaml && k create -f kubernetes-actions/custometaskrun.yaml

tkn taskrun delete kubectl-run && kubectl create -f kubernetes-actions/tstrun.yaml

kubectl create configmap kubeconfig --from-file=k3sconfig

kubectl apply -f kubernetes-actions/customepipeline.yaml

tkn pipelinerun delete custome-run && k apply -f kubernetes-actions/customepipeline.yaml && k create -f kubernetes-actions/customepipelinerun.yaml

jq  '.' expertjob-online-tekton/deploy.yaml
echo "remotejob/tekton-tutorial:0.0.17-prod" | yq '.spec.template.spec.containers[0].image' expertjob-online-tekton/deploy.yaml

yq e  '.spec.template.spec.containers[0].image="remotejob/tekton-tutorial:0.0.17-prod"' expertjob-online-tekton/deploy.

k apply -f tekton-triggers-org/trigger-template-dochub.yaml



```



